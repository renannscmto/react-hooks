import logo from './logo.svg';
import './App.css';

import { useState } from 'react';

function App() {
  const [spin, setSpin] = useState(false);
  const [counter, setCounter] = useState(0);

  const handleReverseLogo = () => {
    setSpin(!spin);
  };

  const handleIncrementCounter = () => {
    setCounter(counter + 1);
  };

  const isReverse = spin ? 'reverse' : '';

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className={`App-logo ${isReverse}`} alt="logo" />

        <p> {isReverse ? 'Reverse' : 'No Reverse'} </p>
        <p> Counter: {counter} </p>

        <div>
          <button type="button" onClick={handleReverseLogo}>
            Click to Reverse Spin
          </button>
        </div>

        <div>
          <button type="button" onClick={handleIncrementCounter}>
            Click to Increment Counter
          </button>
        </div>
      </header>
    </div>
  );
}

export default App;
