import './App.css';

import { useState, useEffect } from 'react';

const actionFn = () => {
  console.log('h1 clicado');
};

function App() {
  const [counter, setCounter] = useState(0);

  const handleCounter = () => {
    setCounter((prevState) => prevState + 1);
  };

  // Executada somente 1x pois existe um lista de depêndencias vazia.
  useEffect(() => {
    document.querySelector('h1')?.addEventListener('click', actionFn);

    // ComponentWillUnmount - Limpa os dados ao desmontar o componente.
    return () => {
      document.querySelector('h1').removeEventListener('click', actionFn);
    };
  }, []);

  // ComponentDidUpdate - Executa toda vez que o estado do componente atualizar, sem dependências.
  useEffect(() => {
    console.log('componentDidUpdate');
  });

  // Com dependência - Executa quando o estado passado na dependência mudar.
  useEffect(() => {
    console.log('Counter:', counter);
  }, [counter]);

  return (
    <div className="App">
      <h1>Counter: {counter}</h1>
      <button type="button" onClick={handleCounter}>
        Click to Increment Counter
      </button>
    </div>
  );
}

export default App;
