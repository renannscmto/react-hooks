useState:
    > Usar este estado quando o componente precisar lembrar informações.
    > O estado é privado do componente, cada cópia de um componente possui seu próprio estado.
    > Este estado possui um array de duas posições, onde o primeiro indice contém o valor do estado e o segundo indice é uma função setter que altera o próprio estado.

useEffect:
    > Este hook oferece o mesmo trabalho de ComponentDidAmount, ComponentDidUpdate e ComponentWillUnmount, funções que são lidas ao Montar, Atualizar e Desmontar o componente.

useCallback:
    > Permite armazenar em cache uma definição de função entre re-renderizações, caso suas dependencias se passadas forem alteradas.
    > Geralmente é usado o useCallback juntamente com o useMemo, para que o componente dentro de useMemeo seja guardado em memória juntamente com a função useCallback que este mesmo componente usa.
