import React, { useState, useCallback } from 'react';
import './App.css';
import P from 'prop-types';

// Guardando o componente em memória com 'React.memo'
const Button = React.memo(function Button({ incrementButton }) {
  console.log('Component soon changed');
  return <button onClick={() => incrementButton(1)}> + </button>;
});

// Tipando as props do componente 'Button' que neste caso recebe apenas uma função.
Button.propTypes = {
  incrementButton: P.func.isRequired,
};

function App() {
  const [counter, setCounter] = useState(0);

  const handleIncrement = useCallback((number) => {
    // Neste caso, para que não seja necessário passar o 'counter' como dependência usamos sua referência em uma função anônima com 'prevCounter' , caso contrário a função irá ser chamada toda vez que 'counter' mudar.
    setCounter((prevCounter) => prevCounter + number);
  }, []);

  console.log('Component father changed');

  return (
    <div className="App">
      <h1>Counter {counter}</h1>
      <Button incrementButton={handleIncrement} />
    </div>
  );
}

export default App;
